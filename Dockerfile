FROM ubuntu:20.04
LABEL maintainer="info@axonibyte.com"
RUN apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install curl jq bash -y \
    && addgroup --gid 2000 chainsvc \
    && useradd --no-create-home chainsvc -s /bin/false -u 2000 -g chainsvc
COPY build/kava /bin/kava
RUN chmod +x /bin/kava
STOPSIGNAL SIGTERM
USER chainsvc
WORKDIR /chain
ENTRYPOINT ["/bin/kava", "start", "--home", "/chain"]
